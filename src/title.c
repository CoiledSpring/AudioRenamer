#include <stdio.h>
#include <string.h>
#include "title.h"

void get_title(char * title, int n, char * expr, char * filebase, struct Tags * tags) {
     int i = 0;
     memset(title, 0, n);
     strncpy(title, filebase, strrchr(filebase, '/') - filebase + 1);
     for(i = 0; i < strlen(expr); i++) {
	  switch(expr[i]) {
	  case '%':
	       i++;
	       switch(expr[i]) {
	       case 'a':
		    strcpy(title + strlen(title), tags->artist);
		    break;
	       case 't':
		    strcpy(title + strlen(title), tags->title );
		    break;
	       case 'c':
		    strcpy(title + strlen(title), tags->album );
		    break;
	       case 'g':
		    strcpy(title + strlen(title), tags->genre );
		    break;
	       case 'y':
		    sprintf(title + strlen(title), "%d", tags->year);
		    break;
	       case 'i':
		    sprintf(title + strlen(title), "%d", tags->id);
		    break;
	       case '%':
		    title[strlen(title)] = '%';
		    break;
	       default:
		    fprintf(stderr, "WARNING : The %%%c option does not exist.\n", expr[i]);
	       }
	       break;
	  default:
	       title[strlen(title)] = expr[i];
	  }
     }
     strcpy(title + strlen(title), strrchr(filebase, '.'));
}

#ifndef TAGS_H
#define TAGS_H
struct Tags {
     char * artist;
     char * title ;
     char * album ;
     char * genre ;
     int    year  ;
     int    id    ;
};

struct Tags get_tags(char * file);
#endif /* TAGS_H */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <errno.h>

#include "tags.h"
#include "title.h"

int main(int argc, char ** argv) {
     char * expr = NULL, * buffer = malloc(255);
     char c;
     bool verbose = false, simulation = false;
     int index;
     struct Tags tags;
     
     while((c = getopt(argc, argv, "vshe:")) != (-1)) {
	  switch(c) {
	  case 'v':
	       verbose = true;
	       break;
	  case 's':
	       simulation = true;
	       break;
	  case 'h':
	       printf("Usage : ./%s [-v] [-h] -e expr files  \n"
		      "Arguments :                           \n"
		      "    -v Verbose mode                   \n"
		      "    -h Print this message             \n"
		      "    -e The expression :               \n"
		      "        %%a  The artist               \n"
		      "        %%t  The title                \n"
		      "        %%c  The album                \n"
		      "        %%g  The genre                \n"
		      "        %%y  The year                 \n"
		      "        %%i  Track ID                 \n");
	       return 0;
	  case 'e':
	       expr = optarg;
	       break;
	  case '?':
	       if(optopt == 'e') {
		    fprintf(stderr, "The -e argument needs an argument.\n");
		    return 1;
	       } else {
		    return 1;
	       }
	  }
     }

     if(expr == NULL) {
	  fprintf(stderr, "The -e argument is necessary.\n");
     }

     for(index = optind; index < argc; index++) {
	  tags = get_tags(argv[index]);
	  get_title(buffer, 255, expr, argv[index], &tags);
	  if(verbose) {
	       printf("« %s » -> « %s »\n", argv[index], buffer);
	  }
	  if(!simulation) {
	       if(rename(argv[index], buffer) == -1) {
		    fprintf(stderr, "Error %d while renaming the file « %s » to « %s »\n", errno, argv[index], buffer);
	       }
	  }
     }
}

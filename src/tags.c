#include <taglib/tag_c.h>
#include <string.h>

#include "tags.h"

static inline void escape(char * s) {
     char * c;
     while((c = strchr(s, '/'))) {
	  (*c) = '_';
     }
}

struct Tags get_tags(char * file) {
     TagLib_File * file_tag;
     TagLib_Tag  * tag_from;
     struct Tags tags;

     file_tag = taglib_file_new(file);
     tag_from = taglib_file_tag(file_tag);
     
     tags.artist = taglib_tag_artist(tag_from);
     escape(tags.artist);
     
     tags.title  = taglib_tag_title (tag_from);
     escape(tags.title );
     
     tags.album  = taglib_tag_album (tag_from);
     escape(tags.album );

     tags.genre  = taglib_tag_genre (tag_from);
     escape(tags.genre );

     tags.year   = taglib_tag_year  (tag_from);
     
     tags.id     = taglib_tag_track (tag_from);

     return tags;
}

void free_tags(struct Tags tags) {
     taglib_free(tags.artist);
     taglib_free(tags.title );
     taglib_free(tags.album );
}
